import React from 'react'
import { act } from 'react-test-renderer'
import { mountWithStore } from '../../../testUtils/utils.spec'

import { defaultAuthState } from '../../Store/authReducer/auth.reducer'
import { defaultToyListState } from '../../Store/toyListReducer/toyList.reducer'
import * as services from '../../Services/api.service'

import List from './List'
import ToyList from './components/ToyList'
import { TOYS } from '../../Store/toyListReducer/toyList.actions'

jest.mock('./components/ToyList', () => () => <div />)

describe('<List />', () => {
  let store
  let renderer

  afterEach(() => {
    store.clearActions()
    renderer.unmount()
  })

  function initRenderer({ isLoaded }) {
    const { _renderer, _store } = mountWithStore(
      {
        authReducer: defaultAuthState,
        toyListReducer: { ...defaultToyListState, isLoaded },
      },
      List,
    )
    renderer = _renderer
    store = _store
  }

  it('should render', (done) => {
    services.getToys = jest.fn(() => Promise.resolve({ data: ['hop'] }))

    act(() => initRenderer({ isLoaded: false }))

    setTimeout(() => {
      expect(services.getToys).toBeCalled()
      expect(store.getActions()).toContainEqual({
        type: TOYS.GET_TOYS_SUCCESS,
        toys: ['hop'],
      })
      done()
    })
  })

  it('should select a toy', () => {
    services.getToys = jest.fn(() => Promise.resolve({ data: [{ id: 7 }] }))

    act(() => initRenderer({ isLoaded: true }))

    const instance = renderer.root
    const list = instance.findByType(ToyList)

    list.props.handleClick(7)
    expect(store.getActions()).toContainEqual({
      type: TOYS.SELECT_TOY,
      id: 7,
    })
  })
})
