import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { getToysAction, selectToyAction } from '../../Store/toyListReducer/toyList.actions'
import types from './models/toyType'

import ToyList from './components/ToyList'

const propTypes = {
  isLoaded: PropTypes.bool.isRequired,
  toys: PropTypes.arrayOf(PropTypes.shape(types.toyType)).isRequired,
  selectToy: PropTypes.func.isRequired,
  getToys: PropTypes.func.isRequired,
}

const List = ({
  isLoaded, toys, getToys, selectToy,
}) => {
  useEffect(() => {
    if (isLoaded) return
    getToys()
  }, [isLoaded, getToys])

  const handleClick = id => selectToy(id)

  return <ToyList toys={toys} handleClick={handleClick} />
}

List.propTypes = propTypes

const mapStateToProps = ({ toyListReducer }) => ({
  isLoaded: toyListReducer.isLoaded,
  toys: toyListReducer.toys,
})

export default connect(mapStateToProps, {
  getToys: getToysAction,
  selectToy: selectToyAction,
})(List)
