import PropTypes from 'prop-types'

const toyType = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  selected: PropTypes.bool,
}

export default {
  toyType,
}
