import React from 'react'
import TestRenderer from 'react-test-renderer'
import theme from '../../../theme'
import 'jest-styled-components'

import { StyledToyButton } from './styled'

describe('<StyledToyButton />', () => {
  let renderer
  let button

  function initRenderer({ isSelected }) {
    renderer = TestRenderer.create(
      <StyledToyButton selected={isSelected} theme={theme} />,
    )
    button = renderer.toJSON()
  }

  afterEach(() => {
    renderer.unmount()
  })

  it('should render selected', () => {
    initRenderer({ isSelected: true })
    expect(button).toHaveStyleRule('border-color', theme.colors.yellow)
    expect(button).toHaveStyleRule('color', theme.colors.white)
  })

  it('should render not selected', () => {
    initRenderer({ isSelected: false })
    expect(button).toHaveStyleRule('border-color', theme.colors.white)
  })
})
