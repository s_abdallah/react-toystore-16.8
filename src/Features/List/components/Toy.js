import React from 'react'
import PropTypes from 'prop-types'
import types from '../models/toyType'
import { StyledToyButton } from './styled'
import '../../../../node_modules/@mdi/font/css/materialdesignicons.min.css'

const propTypes = {
  ...types.toyType,
  handleClick: PropTypes.func.isRequired,
}

const Toy = ({
  handleClick, id, selected, icon,
}) => {
  const iconClass = `mdi mdi-${icon}`

  return (
    <StyledToyButton
      onClick={() => handleClick(id)}
      selected={selected}
      icon={icon}
    >
      <i className={iconClass} />
    </StyledToyButton>
  )
}

Toy.propTypes = propTypes

export default React.memo(Toy, (prev, next) => prev.selected === next.selected)
