import React from 'react'
import TestRenderer, { act } from 'react-test-renderer'

import Toy from './Toy'
import { StyledToyButton } from './styled'

jest.mock('./styled', () => ({
  StyledToyButton: () => <div />,
}))

describe('<Toy />', () => {
  it('should render', () => {
    const mockClick = jest.fn()

    let renderer
    act(() => {
      renderer = TestRenderer.create(
        <Toy
          handleClick={mockClick}
          id={1}
          title="titre"
          icon="image"
          price={10}
          selected
        />,
      )
    })

    const instance = renderer.root
    const button = instance.findByType(StyledToyButton)
    const icone = button.props.children

    expect(button.props.selected).toBe(true)
    expect(button.props.icon).toBe('image')
    expect(icone.props.className).toBe('mdi mdi-image')

    button.props.onClick()
    expect(mockClick).toBeCalledWith(1)
  })
})
