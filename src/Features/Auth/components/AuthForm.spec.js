import React from 'react'
import TestRenderer, { act } from 'react-test-renderer'
import { ThemeProvider } from 'styled-components'
import theme from '../../../theme'
import { mockFocusedElement } from '../../../../testUtils/utils.spec'

import AuthForm from './AuthForm'
import {
  AuthBackground, AuthBox, AuthInput, AuthMessage, AuthButton,
} from './styled'

describe('<AuthForm />', () => {
  let renderer
  const close = jest.fn()
  const submit = jest.fn()

  function initRenderer({ isInvalid }) {
    renderer = TestRenderer.create(
      <ThemeProvider theme={theme}>
        <AuthForm
          close={close}
          submit={submit}
          isInvalid={isInvalid}
        />
      </ThemeProvider>,
      mockFocusedElement,
    )
  }

  afterEach(() => {
    renderer.unmount()
  })

  it('should render with invalid', () => {
    act(() => initRenderer({ isInvalid: true }))

    const instance = renderer.root
    const input = instance.findAllByType(AuthInput)
    const message = instance.findByType(AuthMessage)

    expect(input[0].props.isInvalid).toBe(true)
    expect(input[1].props.isInvalid).toBe(true)

    expect(input[0].props.value).toBe('')
    expect(input[1].props.value).toBe('')

    expect(message.props.isVisible).toBe(true)
  })

  it('should render with valid', () => {
    act(() => initRenderer({ isInvalid: false }))

    const instance = renderer.root
    const input = instance.findAllByType(AuthInput)
    const message = instance.findByType(AuthMessage)

    expect(input[0].props.isInvalid).toBe(false)
    expect(input[1].props.isInvalid).toBe(false)

    expect(message.props.isVisible).toBe(false)
  })

  it('should close', () => {
    act(() => initRenderer({ isInvalid: false }))

    const instance = renderer.root
    const bg = instance.findByType(AuthBackground)
    bg.props.onClick()

    expect(close).toBeCalled()
  })

  it('should prevent from closing', () => {
    const preventDefault = jest.fn()
    const stopPropagation = jest.fn()

    act(() => initRenderer({ isInvalid: false }))

    const instance = renderer.root
    const box = instance.findByType(AuthBox)
    box.props.onClick({
      preventDefault,
      stopPropagation,
    })

    expect(preventDefault).toBeCalled()
    expect(stopPropagation).toBeCalled()
  })

  it('should submit credentials', () => {
    act(() => initRenderer({ isInvalid: false }))

    const instance = renderer.root
    const email = instance.findByProps({ type: 'email' })
    const pass = instance.findByProps({ type: 'password' })
    const button = instance.findByType(AuthButton)

    act(() => {
      email.props.onChange({ target: { value: 'hop' } })
    })
    act(() => {
      pass.props.onChange({ target: { value: 'secret' } })
    })

    button.props.onClick()

    expect(submit).toBeCalledWith('hop', 'secret')
  })
})
