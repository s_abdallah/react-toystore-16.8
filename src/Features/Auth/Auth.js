import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { connectAction, closeAction } from '../../Store/authReducer/auth.actions'

import AuthRedirect from './components/AuthRedirect'
import AuthForm from './components/AuthForm'

const propTypes = {
  isConnected: PropTypes.bool.isRequired,
  isInvalid: PropTypes.bool.isRequired,
  auth: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
}

const Auth = ({
  isConnected, isInvalid, auth, close,
}) => {
  const handleSubmit = (email, pass) => {
    auth(email, pass)
  }

  return isConnected ? <AuthRedirect close={close} />
    : (
      <AuthForm
        close={close}
        isInvalid={isInvalid}
        submit={handleSubmit}
      />
    )
}

Auth.propTypes = propTypes

const mapStateToProps = ({ authReducer }) => ({
  isConnected: authReducer.isConnected,
  isInvalid: authReducer.isInvalid,
})

export default connect(mapStateToProps, {
  auth: connectAction,
  close: closeAction,
})(Auth)
