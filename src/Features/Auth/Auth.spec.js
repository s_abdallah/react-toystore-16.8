import React from 'react'
import { act } from 'react-test-renderer'
import { mountWithStoreAndRouter } from '../../../testUtils/utils.spec'
import * as services from '../../Services/api.service'

import { defaultAuthState } from '../../Store/authReducer/auth.reducer'
import { defaultToyListState } from '../../Store/toyListReducer/toyList.reducer'

import Auth from './Auth'
import AuthRedirect from './components/AuthRedirect'
import AuthForm from './components/AuthForm'
import { AUTH } from '../../Store/authReducer/auth.actions'

jest.mock('./components/AuthRedirect', () => () => <div />)
jest.mock('./components/AuthForm', () => () => <div />)

describe('<Auth />', () => {
  let store
  let renderer

  afterEach(() => {
    renderer.unmount()
    store.clearActions()
  })

  function initRenderer({ isConnected, isInvalid }) {
    const { _renderer, _store } = mountWithStoreAndRouter(
      {
        authReducer: { ...defaultAuthState, isConnected, isInvalid },
        toyListReducer: { ...defaultToyListState },
      },
      Auth,
    )
    renderer = _renderer
    store = _store
  }

  it('should render with redirect', () => {
    act(() => initRenderer({ isConnected: true, isInvalid: false }))

    const instance = renderer.root
    const redirect = instance.findByType(AuthRedirect)

    redirect.props.close()

    expect(store.getActions()).toContainEqual({
      type: AUTH.CLOSE,
    })
  })

  it('should render with form', (done) => {
    services.authentication = jest.fn(() => Promise.resolve({ data: [] }))

    act(() => initRenderer({ isConnected: false, isInvalid: true }))

    const instance = renderer.root
    const form = instance.findByType(AuthForm)

    expect(form.props.isInvalid).toBe(true)

    form.props.close()
    expect(store.getActions()).toContainEqual({
      type: AUTH.CLOSE,
    })

    form.props.submit('toto', 'pan')

    setTimeout(() => {
      expect(services.authentication).toBeCalledWith('toto', 'pan')
      expect(store.getActions()).toContainEqual({
        type: AUTH.CONNECT_ERROR,
      })

      done()
    })
  })
})
