import React from 'react'
import TestRenderer, { act } from 'react-test-renderer'

import BasketList from './BasketList'
import BasketToy from './BasketToy'

jest.mock('./BasketToy', () => () => <div />)
describe('<BasketList />', () => {
  const mockClick = jest.fn()
  const toys = [
    {
      id: 1, price: 10, selected: true, title: '', icon: '',
    },
    {
      id: 2, price: 20, selected: true, title: '', icon: '',
    },
  ]

  it('should render', () => {
    let renderer
    act(() => {
      renderer = TestRenderer.create(
        <BasketList
          handleClick={mockClick}
          toys={toys}
        />,
      )
    })

    const instance = renderer.root
    const list = instance.findAllByType(BasketToy)
    const toy = list[0]

    expect(list.length).toBe(2)

    toy.props.handleClick()
    expect(mockClick).toBeCalled()
  })
})
