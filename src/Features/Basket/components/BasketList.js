import React from 'react'
import PropTypes from 'prop-types'

import BasketToy from './BasketToy'
import { StyledToyColumn } from './styled'

import types from '../../List/models/toyType'

const propTypes = {
  toys: PropTypes.arrayOf(PropTypes.shape(types.toyType)).isRequired,
  handleClick: PropTypes.func.isRequired,
}

const BasketList = ({
  handleClick, toys,
}) => (
  <StyledToyColumn>
    {toys.map(toy => <BasketToy key={toy.id} handleClick={handleClick} toy={toy} />)}
  </StyledToyColumn>
)


BasketList.propTypes = propTypes

export default BasketList
