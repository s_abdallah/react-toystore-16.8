import React from 'react'
import { PropTypes } from 'prop-types'
import { StyledPrice, StyledToyColumn } from './styled'

const propTypes = {
  price: PropTypes.number.isRequired,
}

const BasketPrice = ({ price }) => (
  <StyledToyColumn>
    <StyledPrice>
      {price}
      &euro;
    </StyledPrice>
  </StyledToyColumn>
)

BasketPrice.propTypes = propTypes

export default BasketPrice
