import React from 'react'
import TestRenderer, { act } from 'react-test-renderer'
import { ThemeProvider } from 'styled-components'
import theme from '../../../theme'

import BasketToy from './BasketToy'
import { StyledToyWrapper } from './styled'

describe('<BasketToy />', () => {
  it('should render', () => {
    let renderer
    const mockClick = jest.fn()

    const toy = {
      id: 7,
      title: 'hop',
      price: 10,
      icon: '',
    }

    act(() => {
      renderer = TestRenderer.create(
        <ThemeProvider theme={theme}>
          <BasketToy
            toy={toy}
            handleClick={mockClick}
          />
        </ThemeProvider>,
      )
    })

    const instance = renderer.root
    const allSpans = instance.findAllByType('span')

    expect(allSpans[1].props.children).toBe('hop')
    expect(allSpans[2].props.children).toEqual([10, '€'])

    const wrapper = instance.findByType(StyledToyWrapper)
    wrapper.props.onClick()

    expect(mockClick).toBeCalledWith(7)
  })
})
