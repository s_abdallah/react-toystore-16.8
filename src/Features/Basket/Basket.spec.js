import React from 'react'
import { act } from 'react-test-renderer'
import { mountWithStore } from '../../../testUtils/utils.spec'

import { defaultAuthState } from '../../Store/authReducer/auth.reducer'
import { defaultToyListState } from '../../Store/toyListReducer/toyList.reducer'
import * as services from '../../Services/api.service'

import BasketList from './components/BasketList'
import BasketPrice from './components/BasketPrice'
import { TOYS } from '../../Store/toyListReducer/toyList.actions'

import Basket from './Basket'

jest.mock('./components/BasketList', () => () => <div />)
jest.mock('./components/BasketPrice', () => () => <div />)

describe('<Basket />', () => {
  let store
  let renderer

  const toys = [
    {
      id: 1, price: 10, selected: true, title: '', icon: '',
    },
    {
      id: 2, price: 20, selected: true, title: '', icon: '',
    },
    {
      id: 3, price: 50, selected: false, title: '', icon: '',
    },
  ]

  afterEach(() => {
    store.clearActions()
    renderer.unmount()
  })

  function initRenderer() {
    const { _renderer, _store } = mountWithStore(
      {
        authReducer: defaultAuthState,
        toyListReducer: { ...defaultToyListState, toys, isLoaded: true },
      },
      Basket,
    )
    renderer = _renderer
    store = _store
  }

  it('should render', () => {
    act(() => initRenderer())
    const instance = renderer.root
    const list = instance.findByType(BasketList)
    const price = instance.findByType(BasketPrice)

    expect(list.props.toys).toEqual([
      {
        id: 1, price: 10, selected: true, title: '', icon: '',
      },
      {
        id: 2, price: 20, selected: true, title: '', icon: '',
      },
    ])
    expect(price.props.price).toEqual(30)
  })

  it('should select a toy', () => {
    services.getToys = jest.fn(() => Promise.resolve({ data: [{ id: 7 }] }))
    act(() => initRenderer())

    const instance = renderer.root
    const list = instance.findByType(BasketList)

    list.props.handleClick(1)
    expect(store.getActions()).toContainEqual({
      type: TOYS.SELECT_TOY,
      id: 1,
    })
  })
})
