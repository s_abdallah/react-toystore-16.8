import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { selectToyAction } from '../../Store/toyListReducer/toyList.actions'
import types from '../List/models/toyType'

import BasketList from './components/BasketList'
import BasketPrice from './components/BasketPrice'

import { StyledBasketWrapper } from './components/styled'

const propTypes = {
  selectedToys: PropTypes.arrayOf(PropTypes.shape(types.toyType)).isRequired,
  selectToy: PropTypes.func.isRequired,
  price: PropTypes.number.isRequired,
}

const Basket = ({ selectedToys, price, selectToy }) => {
  const handleClick = id => selectToy(id)

  return (
    <StyledBasketWrapper>
      <BasketList toys={selectedToys} handleClick={handleClick} />
      <BasketPrice price={price} />
    </StyledBasketWrapper>
  )
}

Basket.propTypes = propTypes

const mapStateToProps = ({ toyListReducer }) => ({
  selectedToys: toyListReducer.toys.filter(toy => toy.selected),
  price: toyListReducer.toys
    .filter(toy => toy.selected)
    .reduce((totalPrice, toy) => (
      totalPrice + toy.price
    ), 0),
})

export default connect(mapStateToProps, {
  selectToy: selectToyAction,
})(Basket)
