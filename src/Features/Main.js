import React from 'react'
import {
  BrowserRouter, Switch, Route, Redirect,
} from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Header from './Header/Header'
import List from './List/List'
import Basket from './Basket/Basket'
import Auth from './Auth/Auth'

const propTypes = {
  isOpened: PropTypes.bool.isRequired,
}

const Main = ({ isOpened }) => (
  <BrowserRouter>
    <React.Fragment>
      <Header />
      <Switch>
        <Route exact path="/" component={List} />
        <Route path="/basket" component={Basket} />
        <Redirect to="/" />
      </Switch>
      {isOpened && <Auth />}
    </React.Fragment>
  </BrowserRouter>
)

Main.propTypes = propTypes

const mapStateToProps = ({ authReducer }) => ({
  isOpened: authReducer.isOpened,
})
export default connect(mapStateToProps)(Main)
