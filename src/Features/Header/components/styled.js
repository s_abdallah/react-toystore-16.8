import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const StyledHeaderWrapper = styled.header`
  align-items: center;
  background: ${props => props.theme.colors.black};
  display: flex;
  justify-content: space-between;
  padding: 0.75rem 2rem;
`

export const StyledHeaderLink = styled(Link)`
  color: ${props => props.theme.colors.white} !important;
  font-size: 1.2rem;
  letter-spacing: 0.2rem;
  margin: 0;
  text-decoration: none;
  text-transform: uppercase;
`

export const StyledHeaderBasket = styled.aside`
  align-items: center;
  background: ${props => props.theme.colors.red};
  border-radius: 50%;
  color: ${props => props.theme.colors.white};
  cursor: pointer;
  display: flex;
  font-size: 1rem;
  font-weight: bold;
  justify-content: center;
  height: 2rem;
  transition: background ease 150ms;
  width: 2rem;
  &:hover {
    background: ${props => props.theme.colors.blue};
  }
`
