import { mountWithStoreAndRouter } from '../../../testUtils/utils.spec'

import { defaultAuthState } from '../../Store/authReducer/auth.reducer'
import { defaultToyListState } from '../../Store/toyListReducer/toyList.reducer'

import Header from './Header'
import { StyledHeaderBasket } from './components/styled'
import { AUTH } from '../../Store/authReducer/auth.actions'

describe('<Header />', () => {
  let store
  let renderer

  afterEach(() => {
    store.clearActions()
    renderer.unmount()
  })

  function initRenderer() {
    const { _renderer, _store } = mountWithStoreAndRouter(
      {
        authReducer: defaultAuthState,
        toyListReducer: { ...defaultToyListState, toys: [{ selected: true }] },
      },
      Header,
    )
    renderer = _renderer
    store = _store
  }

  it('should render', () => {
    initRenderer()

    const instance = renderer.root
    const basket = instance.findByType(StyledHeaderBasket)

    basket.props.onClick()
    expect(store.getActions()).toContainEqual({
      type: AUTH.OPEN,
    })

    expect(basket.props.children).toBe(1)
  })
})
