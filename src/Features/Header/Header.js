import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  StyledHeaderWrapper, StyledHeaderLink, StyledHeaderBasket,
} from './components/styled'
import { openAction } from '../../Store/authReducer/auth.actions'

const propTypes = {
  nbSelected: PropTypes.number.isRequired,
  openAction: PropTypes.func.isRequired,
}

const Header = (props) => {
  const { nbSelected } = props

  const handleClick = () => {
    props.openAction()
  }

  return (
    <StyledHeaderWrapper>
      <StyledHeaderLink to="/">Toy Store</StyledHeaderLink>
      <StyledHeaderBasket onClick={handleClick}>{nbSelected}</StyledHeaderBasket>
    </StyledHeaderWrapper>
  )
}

Header.propTypes = propTypes
const mapStateToProps = ({ toyListReducer }) => ({
  nbSelected: toyListReducer.toys.filter(toy => toy.selected).length,
})
export default connect(mapStateToProps, {
  openAction,
})(Header)
