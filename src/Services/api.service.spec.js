import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import * as services from './api.service'

describe('api service', () => {
  let mock

  beforeEach(() => {
    mock = new MockAdapter(axios)
  })

  afterEach(() => {
    mock.reset()
  })

  it('should authenticate', () => {
    const userReq = 'http://localhost:9000/users?user=toto&pass=pan'
    mock.onGet(userReq).reply(200, ['hop'])

    return services.authentication('toto', 'pan').then((res) => {
      expect(res.data).toEqual(['hop'])
    })
  })

  it('should get toys', () => {
    const userReq = 'http://localhost:9000/toys'
    mock.onGet(userReq).reply(200, ['toy'])

    return services.getToys().then((res) => {
      expect(res.data).toEqual(['toy'])
    })
  })
})
