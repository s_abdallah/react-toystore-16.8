import axios from 'axios'

const baseUrl = 'http://localhost:9000'

export const getToys = () => (
  axios.get(`${baseUrl}/toys`)
)

export const authentication = (user, pass) => (
  axios.get(`${baseUrl}/users?user=${user}&pass=${pass}`)
)
