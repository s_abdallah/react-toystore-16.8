import { AUTH } from './auth.actions'
import authReducer, { defaultAuthState } from './auth.reducer'

describe('authReducer', () => {
  it('should return default state', () => {
    const result = authReducer({ ...defaultAuthState }, {
      type: 'hop',
    })

    expect(result).toEqual(defaultAuthState)
  })

  it('should open', () => {
    const result = authReducer({ ...defaultAuthState }, {
      type: AUTH.OPEN,
    })

    expect(result).toEqual({
      ...defaultAuthState,
      isOpened: true,
      isInvalid: false,
    })
  })

  it('should close', () => {
    const result = authReducer({ ...defaultAuthState }, {
      type: AUTH.CLOSE,
    })

    expect(result).toEqual({
      ...defaultAuthState,
      isOpened: false,
    })
  })

  it('should connect', () => {
    const result = authReducer({ ...defaultAuthState }, {
      type: AUTH.CONNECT,
    })

    expect(result).toEqual({
      ...defaultAuthState,
      isConnected: true,
    })
  })

  it('should error', () => {
    const result = authReducer({ ...defaultAuthState }, {
      type: AUTH.CONNECT_ERROR,
    })

    expect(result).toEqual({
      ...defaultAuthState,
      isInvalid: true,
    })
  })
})
