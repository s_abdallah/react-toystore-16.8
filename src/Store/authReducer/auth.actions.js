import { authentication } from '../../Services/api.service'

export const AUTH = {
  OPEN: 'AUTH/OPEN',
  CLOSE: 'AUTH/CLOSE',
  CONNECT: 'AUTH/CONNECT',
  CONNECT_ERROR: 'AUTH/CONNECT_ERROR',
}

export const openAction = () => ({
  type: AUTH.OPEN,
})

export const closeAction = () => ({
  type: AUTH.CLOSE,
})

export const connectAction = (user, pass) => dispatch => (
  authentication(user, pass).then((res) => {
    if (!res.data.length) {
      dispatch({ type: AUTH.CONNECT_ERROR })
      return
    }

    dispatch({ type: AUTH.CONNECT })
  })
)
