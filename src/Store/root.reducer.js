import { combineReducers } from 'redux'

import authReducer, { defaultAuthState } from './authReducer/auth.reducer'
import toyListReducer, { defaultToyListState } from './toyListReducer/toyList.reducer'

export const defaultRootState = {
  toyListReducer: defaultToyListState,
  authReducer: defaultAuthState,
}

const rootReducer = combineReducers({
  toyListReducer,
  authReducer,
})

export default rootReducer
