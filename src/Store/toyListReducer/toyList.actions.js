import { getToys } from '../../Services/api.service'

export const TOYS = {
  GET_TOYS_SUCCESS: 'TOYS/GET_TOYS_SUCCESS',
  SELECT_TOY: 'TOYS/SELECT_TOY',
}

const populateToysAction = toys => ({
  type: TOYS.GET_TOYS_SUCCESS,
  toys,
})

export const selectToyAction = id => ({
  type: TOYS.SELECT_TOY,
  id,
})

export const getToysAction = () => dispatch => (
  getToys().then((response) => {
    dispatch(populateToysAction(response.data))
  })
)
