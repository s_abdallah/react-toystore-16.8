import * as services from '../../Services/api.service'
import * as actions from './toyList.actions'

describe('toyList.actions', () => {
  it('should select toy', () => {
    const result = actions.selectToyAction(1)
    expect(result).toEqual({
      type: actions.TOYS.SELECT_TOY,
      id: 1,
    })
  })

  it('should get toys', () => {
    const dispatch = jest.fn()
    const data = [
      { id: 1 }, { id: 2 },
    ]
    services.getToys = jest.fn(() => Promise.resolve({ data }))

    const result = actions.getToysAction()

    return result(dispatch).then(() => {
      expect(dispatch).toBeCalledWith({
        type: actions.TOYS.GET_TOYS_SUCCESS,
        toys: data,
      })
    })
  })
})
