import { TOYS } from './toyList.actions'

export const defaultToyListState = {
  toys: [],
  isLoaded: false,
}

const toyListReducer = (state = defaultToyListState, action) => {
  let nextToys = []

  switch (action.type) {
    case TOYS.GET_TOYS_SUCCESS:
      return { ...state, toys: action.toys, isLoaded: true }

    case TOYS.SELECT_TOY:
      nextToys = Array.from(state.toys)

      return {
        ...state,
        toys: nextToys.map((toy) => {
          const nextToy = toy
          if (nextToy.id === action.id) {
            nextToy.selected = !nextToy.selected
          }
          return nextToy
        }),
      }

    default:
      return state
  }
}

export default toyListReducer
