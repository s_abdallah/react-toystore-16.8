import React from 'react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { MemoryRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import TestRenderer from 'react-test-renderer'
import theme from '../src/theme'

export const mockFocusedElement = {
  createNodeMock: (element) => {
    if (element.type === 'input') {
      // mock a focus function
      return {
        focus: () => jest.fn(),
      }
    }
    return null
  },
}

export const mountWithStoreAndRouter = (initialState, Component) => {
  const mockStore = configureStore([thunk])
  const store = mockStore(initialState)
  return {
    _renderer: TestRenderer.create(
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <MemoryRouter>
            <Component />
          </MemoryRouter>
        </ThemeProvider>
      </Provider>,
      mockFocusedElement,
    ),
    _store: store,
  }
}

export const mountWithStore = (initialState, Component) => {
  const mockStore = configureStore([thunk])
  const store = mockStore(initialState)
  return {
    _renderer: TestRenderer.create(
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <Component />
        </ThemeProvider>
      </Provider>,
      mockFocusedElement,
    ),
    _store: store,
  }
}
